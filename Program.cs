﻿using System;


namespace Delegate
{
    class Program
    {
 
        delegate string MessageToConsole(string str);
        delegate int SummToConsole(int a, int b);
        delegate int DiffToConsole(int a, int b);
        delegate int FactorialToConsole(int a);
        delegate string SummOfStrings(string a, string b);
        static void Main(string[] args)
        {
            Task_3();



        }

        private static void Task_1()
        {
            MessageToConsole mess = Message;
            MessageToConsole mess1 = Message;
            Console.WriteLine(mess("Делегаты "));
            Console.WriteLine(mess1("это круто "));
            Console.WriteLine(mess("Делегаты ") + mess1("это круто "));
        }

        private static void Task_2()
        {
            SummToConsole summ = CalculateSumm;
            DiffToConsole diff = CalculateDiff;
            FactorialToConsole factorial = CalculateFactorial;
            Console.WriteLine("Summ is: " + summ(3, 5));
            Console.WriteLine("Diffference is: " + diff(3, 5));
            Console.WriteLine("Factorial is: " + factorial(7));

        }
        private static void Task_3()
        {
            SummOfStrings summ = StringsSumm;
            MessageToConsole mess = Message;
            Console.WriteLine(summ("IT", "Hub"));
            Console.WriteLine(mess("Message"));



        }

        private static int CalculateSumm(int a, int b) => a + b;
        private static int CalculateDiff(int a, int b) => a - b;
        private static int CalculateFactorial(int a)
        {
            int _a = a;
            int factorial = 1;
            for (int i = 2; i <= a; i++)
            {
                factorial *= i;
            }
            return factorial;
        }

        private static string Message(string message) => message;
        private static string StringsSumm(string str1, string str2) => str1 + str2;




    }
}
